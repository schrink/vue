<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/users', function (Request $request) {
    return \App\User::all();
});

Route::middleware('auth:api')->post('/users', function (Request $request) {

    \App\User::create(['name' => $request->name, 'email' => time(), 'password'=> '']);
    return \App\User::all();
});